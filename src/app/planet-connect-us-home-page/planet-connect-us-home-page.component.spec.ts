import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanetConnectUsHomePageComponent } from './planet-connect-us-home-page.component';

describe('PlanetConnectUsHomePageComponent', () => {
  let component: PlanetConnectUsHomePageComponent;
  let fixture: ComponentFixture<PlanetConnectUsHomePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanetConnectUsHomePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanetConnectUsHomePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
