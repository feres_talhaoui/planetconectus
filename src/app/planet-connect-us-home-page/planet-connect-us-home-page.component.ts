import {Component, OnInit} from '@angular/core';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'sit-planet-connect-us-home-page',
  templateUrl: './planet-connect-us-home-page.component.html',
  styleUrls: ['./planet-connect-us-home-page.component.css']
})
export class PlanetConnectUsHomePageComponent implements OnInit {
  homePageImage = {
    src: '/assets/img/planet-connect-us/banners/home.jpg',
    srcset2x: '/assets/img/planet-connect-us/banners/home@2x.jpg',
    srcset3x: '/assets/img/planet-connect-us/banners/home@3x.jpg',
    style: { 'margin-top': '125px'}
  };

  richTextRDPole = {
    html : '<div class="row"><div class="col-lg-12" style="text-align: center;margin-top: 20px;" ><p><span style="color: #72327c;font-size: 36px; font-family: \'avenirNext\';">Empowering companies by enabling</span></p>\n' +
    ' <p><span style="color: #4a4a4a;font-size: 36px; font-family: \'avenirNext\';">innovative &nbsp;<span style="color: #72327c;font-size: 36px; font-family: \'avenirNext\';">IoT</span> AND &nbsp;<span style="color: #72327c;font-size: 36px; font-family: \'avenirNext\';">MOBILITY</span> solutions</span></p></div></div>'
  };

  headerSubImage = {
    src: '/assets/img/planet-connect-us/header-sub-image.jpg',
    srcset2x: '/assets/img/planet-connect-us/header-sub-image@2x.jpg',
    srcset3x: '/assets/img/planet-connect-us/header-sub-image@3x.jpg',
    style: {
      'margin-top': '15px'
    }
  };

  whoWeAreImage = {
    src: '/assets/img/planet-connect-us/whoweare.jpg',
    srcset2x: '/assets/img/planet-connect-us/whoweare@2x.jpg',
    srcset3x: '/assets/img/planet-connect-us/whoweare@3x.jpg',
    style: {

    }
  };

  iotImage = {
    src: '/assets/img/planet-connect-us/iot-connect-all-things.jpg',
    srcset2x: '/assets/img/planet-connect-us/iot-connect-all-things@2x.jpg',
    srcset3x: '/assets/img/planet-connect-us/iot-connect-all-things@3x.jpg',
    style: {
      'margin-top': '120px'
    }
  };

  configImageWorldFatestConnected = {
    src: '/assets/img/planet-connect-us/fastestconnected.jpg',
    srcset2x: '/assets/img/planet-connect-us/fastestconnected@2x.jpg',
    srcset3x: '/assets/img/planet-connect-us/fastestconnected@3x.jpg'
  };

  configImageSubTitle = {src: '/assets/img/planet-connect-us/We-creat-extrenal-Re.png', style: {'margin-top': '5%'}};
  configCallToAction = {
    title: {
      text: 'WHO WE ARE',
      style: {'font-family': 'avenirNextDemi', 'color': '#814A89', 'font-size': '24px', 'padding-bottom': '30px', 'padding-top': '25px'}
    },
    content: {
      text: ['Planet Conectus is a company specialized in IOT and mobile.\n' +
      '      We accompany compaines to make thier business by realizing POCs and then indutrialize\n' +
      '      the solution.\n',
        '      Planet Conectus possesses one of the fastest and secure connected objects in the world']
    },
    button: {
      text: 'Learn more', style: {
        'background-color': '#EA771C',
        'float': 'right',
        'color': 'white'
      }
    },
  };

  product1 = {
    src: '/assets/img/planet-connect-us/product-1.jpg',
    srcset2x: '/assets/img/planet-connect-us/product-1@2x.jpg',
    srcset3x: '/assets/img/planet-connect-us/product-1@3x.jpg',
    style: {
      'margin-top': '3%'
    }
  };
  product2 = {
    src: '/assets/img/planet-connect-us/product-2.jpg',
    srcset2x: '/assets/img/planet-connect-us/product-2@2x.jpg',
    srcset3x: '/assets/img/planet-connect-us/product-2@3x.jpg',
    style: {
      'margin-top': '3%'
    }
  };
  product3 = {
    src: '/assets/img/planet-connect-us/product-3.jpg',
    srcset2x: '/assets/img/planet-connect-us/product-3@2x.jpg',
    srcset3x: '/assets/img/planet-connect-us/product-3@3x.jpg',
    style: {
      'margin-top': '3%'
    }
  };

  product4 = {
    src: '/assets/img/planet-connect-us/product-4.jpg',
    srcset2x: '/assets/img/planet-connect-us/product-4@2x.jpg',
    srcset3x: '/assets/img/planet-connect-us/product-4@3x.jpg',
    style: {
      'margin-top': '3%'
    }
  };
  clientDavidson = {
    src: '/assets/img/planet-connect-us/client-davidson.png',
    srcset2x: '/assets/img/planet-connect-us/client-davidson@2x.png',
    srcset3x: '/assets/img/planet-connect-us/client-davidson@3x.png'
  };
  clientOrange = {
    src: '/assets/img/planet-connect-us/client-orange.png',
    srcset2x: '/assets/img/planet-connect-us/client-orange@2x.png',
    srcset3x: '/assets/img/planet-connect-us/client-orange@3x.png'
  };
  clientPlanete = {
    src: '/assets/img/planet-connect-us/planete-connectee.jpg',
    srcset2x: '/assets/img/planet-connect-us/planete-connectee@2x.jpg',
    srcset3x: '/assets/img/planet-connect-us/planete-connectee@3x.jpg'
  };
  clientAbertis = {
    src: '/assets/img/planet-connect-us/client-abertis.png',
    srcset2x: '/assets/img/planet-connect-us/client-abertis@2x.png',
    srcset3x: '/assets/img/planet-connect-us/client-abertis@3x.png'
  };
  clientCarrefour = {
    src: '/assets/img/planet-connect-us/client-carrefour.png',
    srcset2x: '/assets/img/planet-connect-us/client-carrefour@2x.png',
    srcset3x: '/assets/img/planet-connect-us/client-carrefour@3x.png'
  };
  constructor(private titleService: Title) {
  }

  ngOnInit() {
    this.titleService.setTitle('PlanetConectUs');
  }

}
