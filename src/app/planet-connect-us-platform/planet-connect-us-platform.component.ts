import {Component, OnInit} from '@angular/core';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'sit-planet-connect-us-platform',
  templateUrl: './planet-connect-us-platform.component.html',
  styleUrls: ['./planet-connect-us-platform.component.css']
})
export class PlanetConnectUsPlatformComponent implements OnInit {


  simpleImageConnectUs = {
    src: '/assets/img/planet-connect-us/banners/platform.jpg',
    srcset2x: '/assets/img/planet-connect-us/banners/platform@2x.jpg',
    srcset3x: '/assets/img/planet-connect-us/banners/platform@3x.jpg',
    style: { 'margin-top': '125px'}
  };
  beaconImage = {
    src: '/assets/img/planet-connect-us/beacon.png',
    srcset2x: '/assets/img/planet-connect-us/beacon-2.png',
    srcset3x: '/assets/img/planet-connect-us/beacon-3.png',
    style: {'height': '100px', 'width': '100px', 'margin-top': '20px'}
  }
  bluetoothImage = {
    src: '/assets/img/planet-connect-us/bluetooth.png',
    srcset2x: '/assets/img/planet-connect-us/bluetooth-2.png',
    srcset3x: '/assets/img/planet-connect-us/bluetooth-3.png',
    style: {'height': '100px', 'width': '100px', 'margin-top': '20px'}
  }
  productionImage = {
    src: '/assets/img/planet-connect-us/production.png',
    srcset2x: '/assets/img/planet-connect-us/production-2.png',
    srcset3x: '/assets/img/planet-connect-us/production-3.png',
    style: {'height': '100px', 'width': '100px', 'margin-top': '20px'}
  }
  monitoredImage = {
    src: '/assets/img/planet-connect-us/monitored.png',
    srcset2x: '/assets/img/planet-connect-us/monitored-2.png',
    srcset3x: '/assets/img/planet-connect-us/monitored-3.png',
    style: {'height': '100px', 'width': '100px', 'margin-top': '20px'}
  }
  directiveImage = {
    src: '/assets/img/planet-connect-us/directive.png',
    srcset2x: '/assets/img/planet-connect-us/directive-2.png',
    srcset3x: '/assets/img/planet-connect-us/monitored-3.png',
    style: {'height': '100px', 'width': '100px', 'margin-top': '20px'}
  }
  communicationImage = {
    src: '/assets/img/planet-connect-us/communication.png',
    srcset2x: '/assets/img/planet-connect-us/communication-2.png',
    srcset3x: '/assets/img/planet-connect-us/communication-3.png',
    style: {'height': '100px', 'width': '100px', 'margin-top': '20px'}
  }
  securityImage = {
    src: '/assets/img/planet-connect-us/security.png',
    srcset2x: '/assets/img/planet-connect-us/security-2.png',
    srcset3x: '/assets/img/planet-connect-us/security-3.png',
    style: {'height': '100px', 'width': '100px', 'margin-top': '20px'}
  }
  reportingImage = {
    src: '/assets/img/planet-connect-us/reporting.png',
    srcset2x: '/assets/img/planet-connect-us/reporting-2.png',
    srcset3x: '/assets/img/planet-connect-us/reporting-3.png',
    style: {'height': '100px', 'width': '100px', 'margin-top': '20px'}
  }
  tripImage = {
    src: '/assets/img/planet-connect-us/trip.png',
    srcset2x: '/assets/img/planet-connect-us/trip-2.png',
    srcset3x: '/assets/img/planet-connect-us/trip-3.png',
    style: {'height': '100px', 'width': '100px', 'margin-top': '20px'}
  }
  maintenanceImage = {
    src: '/assets/img/planet-connect-us/maintenance.png',
    srcset2x: '/assets/img/planet-connect-us/maintenance-2.png',
    srcset3x: '/assets/img/planet-connect-us/maintenance-3.png',
    style: {'height': '100px', 'width': '100px', 'margin-top': '20px'}
  }
  schedulingImage = {
    src: '/assets/img/planet-connect-us/scheduling.png',
    srcset2x: '/assets/img/planet-connect-us/scheduling-2.png',
    srcset3x: '/assets/img/planet-connect-us/scheduling-3.png',
    style: {'height': '100px', 'width': '100px', 'margin-top': '20px'}
  }
  constructor(private titleService: Title) {
  }

  ngOnInit() {
    this.titleService.setTitle('Platform');
  }

}
