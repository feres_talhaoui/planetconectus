import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PaygateComponent} from './paygate/paygate.component';
import {PlanetConnectUsHomePageComponent} from './planet-connect-us-home-page/planet-connect-us-home-page.component';
import {PlanetConnectUsAboutUsComponent} from './planet-connect-us-about-us/planet-connect-us-about-us.component';
import {PlanetConnectUsPlatformComponent} from "./planet-connect-us-platform/planet-connect-us-platform.component";

import {PartnersComponent} from './partners/partners.component';
import {ContactComponent} from './contact/contact.component';
import {CareersComponent} from './careers/careers.component';
import {IndoorPositionSystemComponent} from './indoor-position-system/indoor-position-system.component';
import { BlogComponent } from './blog/blog.component';
import {ArticleComponent}from './article/article.component'
import { ShowArticleComponent } from './show-article/show-article.component';
const routes: Routes = [
  { path: '', component: PlanetConnectUsHomePageComponent},
  { path: 'about', component: PlanetConnectUsAboutUsComponent},
  { path: 'platform', component: PlanetConnectUsPlatformComponent},
  { path: 'partners', component: PartnersComponent},
  { path: 'contact', component: ContactComponent},
  { path: 'careers', component: CareersComponent},
  { path: 'solutions/paygate', component: PaygateComponent},
  { path: 'solutions/indoor-position-system', component: IndoorPositionSystemComponent},
  {path:'blog',component:BlogComponent,
children:[{path:'',redirectTo:'showarticle',pathMatch:'full'},
{path:'showarticle',component:ShowArticleComponent,children:[{path:'article/:id',component:ArticleComponent}]}
  ]},

];



@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
