import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Article } from '../article';
import { ArticleService } from '../article.service'
import { ActivatedRoute, Router } from '@angular/router'
import { Location } from '@angular/common'
import { ShowArticleComponent } from '../show-article/show-article.component'
import { FormControl, FormGroup } from '@angular/forms'
import { element } from 'protractor';
@Component({
  selector: 'sit-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit, AfterViewInit {
  article = {};
  articleTitle: any;
  introduction: any;
  img_url: any;
  subTitle_1: any;
  subTitle_2: any;
  content_for_subtitle_1: any
  content_for_subtitle_2: any;
  articlearray: Array<any>;
  articleCommentsArray: Array<any>;
  itroduction_section
  subTitle_1_content_section
  subtitle_2_content_section
  form_content = new FormGroup({
    comment_content: new FormControl('')
  })





  constructor(public sh: ShowArticleComponent, public location: Location, public service: ArticleService, public activatedRouter: ActivatedRoute, public router: Router) {
    service.pushId$.subscribe(studentid => {
      this.service.getarticlebyid(studentid).subscribe(res => {
        this.service.getarticlebyid(studentid).subscribe(res => {
          this.article = res
        })
      })
    })
    this.articlearray = this.sh.articleIdArray
  }

  ngAfterViewInit() {

    let introduction_item = document.getElementById("vertical_line_1")
    let content_of_sub_1 = document.getElementById("vertical_line_2")
    let content_of_sub_2 = document.getElementById("vertical_line_3")
    document.getElementById("introduction_section").onmouseenter = function () {/*introduction_item.className="verti_line_red"*/
      //document.querySelector('#vertical_line_1').style["background-color"]='yellow'
      let heigh = 10;
      /* this.inter=setInterval(()=>{
        if(heigh==200){heigh=10;}
        document.querySelector("#vertical_line_1").style["height"]=heigh+"px"
        heigh+=5;
      },100)*/
    }
    //document.getElementById("introduction_section").onmouseleave =function(){/*introduction_item.className="verti_line_green"*/
    //document.querySelector('#vertical_line_1').style["background-color"]='green';
    //document.querySelector("#vertical_line_1").style["height"]=50+"px"
    //clearInterval(this.inter)
    //}
    /*document.getElementById("content_for_subtitle_1_section").onmouseenter =function(){content_of_sub_1.className="verti_line_red"}
    document.getElementById("content_for_subtitle_1_section").onmouseleave =function(){content_of_sub_1.className="verti_line_green"}
    document.getElementById("content_for_subtitle_2_section").onmouseenter =function(){content_of_sub_2.className="verti_line_red"}
    document.getElementById("content_for_subtitle_2_section").onmouseleave =function(){content_of_sub_2.className="verti_line_green"}*/

  }

  ngOnInit() {
    const sid = this.activatedRouter.snapshot.paramMap.get('id');
    this.sh.show_bool = false
    this.itroduction_section = document.getElementById("introduction_section");
    this.subTitle_1_content_section=document.getElementById("content_for_subtitle_1_section");
    this.subtitle_2_content_section=document.getElementById("content_for_subtitle_2_section");

    this.service.getarticlebyid(sid).subscribe(res => {
   
      this.articleTitle = res['articleTitle'];
      this.subTitle_1 = res['subTitle_1']
      this.subTitle_2 = res['subTitle_2']
      this.content_for_subtitle_1 = res['content_for_subtitle_1']
      this.content_for_subtitle_2 = res['content_for_subtitle_2']
      this.img_url = res['img_url'];
      this.introduction = res['introduction']
      /** i) Parsing wysiwig contents  */
      this.introduction = new DOMParser().parseFromString(this.introduction, "text/html")
      this.content_for_subtitle_1 = new DOMParser().parseFromString(this.content_for_subtitle_1, "text/html")
      this.content_for_subtitle_2=new DOMParser().parseFromString(this.content_for_subtitle_2, "text/html")
      //this.content_for_subtitle_2 = new DOMParser().parseFromString(this.content_for_subtitle_2, "text/html").firstElementChild.getElementsByTagName("p")[0].firstChild.textContent
      /**End of parsing */

      /** ii) Iterating the introductoin array and append it's content to introduction article section */
      for (let item of this.introduction.firstElementChild.getElementsByTagName("p")) {
        this.itroduction_section.appendChild(item.firstChild)
      }
      /**End of appending elements */

      /** iii) Iterating first <h2> subtitle content and append elements to subtitle content section  */
     /* if(!this.content_for_subtitle_1.firstElementChild.getElementsByTagName("span")){
        for(let item of this.content_for_subtitle_1.firstElementChild.getElementsByTagName("span")){
          this.subTitle_1_content_section.appendChild(item.firstChild)
        }
      }else{
        for(let item of this.content_for_subtitle_1.firstElementChild.getElementsByTagName("body")[0].childNodes){
          this.subTitle_1_content_section.appendChild(item.firstChild)
        }
      }*/
      for(let item of this.content_for_subtitle_1.firstElementChild.getElementsByTagName("body")[0].childNodes){
        this.subTitle_1_content_section.appendChild(item.firstChild)
      }
      for(let item of this.content_for_subtitle_2.firstElementChild.getElementsByTagName("body")[0].childNodes){
        this.subtitle_2_content_section.appendChild(item.firstChild)
      }
      /** End of appending elements */
      console.log("+",this.content_for_subtitle_1)
      console.log("article",this.content_for_subtitle_2.firstElementChild.getElementsByTagName("body"))

      this.location.subscribe((value: PopStateEvent) => { this.sh.show_bool = true })

    })

    this.service.getCommentsByArticleId(sid).subscribe((res: Array<any>) => {
      this.articleCommentsArray = res
    })
// end of ngOninit
  }

  submitComment() {
    let sid = this.activatedRouter.snapshot.paramMap.get('id');
    this.service.addCommentToArticle(sid, this.form_content.value).subscribe(res => {
    })

  }

}
