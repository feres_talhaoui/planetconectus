import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import {Subject} from 'rxjs';
@Injectable()
export class ArticleService {

  private url="http://localhost:3001/articles";


  private pushIdSource=new Subject<any>()
  pushId$=this.pushIdSource.asObservable();
  constructor(private http:HttpClient) { }

pushIdToArticle(articleid:any){
  this.pushIdSource.next(articleid);
}

  getarticle(){
    return this.http.get(this.url);
  }
  getarticlebyid(studentid:any)
  {
    return this.http.get(this.url+'/'+studentid);
  }
  getCommentsByArticleId(id:string){
    return this.http.get(this.url+'/'+id+'/comments')
  }
  addCommentToArticle(id:string,comment:any){
    return this.http.post(this.url+'/'+id+'/comments',comment)
  }


}


