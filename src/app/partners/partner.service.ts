import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Partner} from './partner';
import {Observable} from 'rxjs/Observable';
import {environment} from '../../environments/environment';

@Injectable()
export class PartnerService {
  apiUrl = environment.apiUrl;

  constructor(private http: HttpClient) { }
  createPartner(partner: Partner): Observable<any> {
    return this.http.post(this.apiUrl + '/Partners', partner);
  }

}
