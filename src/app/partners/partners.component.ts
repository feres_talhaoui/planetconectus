import {Component, OnInit} from '@angular/core';
import {Partner} from './partner';
import {PartnerService} from './partner.service';
import {Contact} from '../contact/Contact';
import {ContactService} from '../contact/contact.service';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'sit-partners',
  templateUrl: './partners.component.html',
  styleUrls: ['./partners.component.css'],
  providers: [PartnerService]

})
export class PartnersComponent implements OnInit {
  simpleTextTitle = {
    paragraphs: [{
      text: 'PARTNERS', style: {
        'letter-spacing': '-0.6px',
        'color': '#72327C',
        'text-align': 'center',
        'font-size': '24px',
        'margin-bottom': '7%'
        ,
        'font-family': 'avenirNextDemi',
        'margin-top': '5%'
      }
    }]
  };
  simpleTextContent = {
    paragraphs: [{
      text: 'We can not innovate without doing research and development, that\'s why planet conectus has an R & D pole made\n' +
      'up of engineers who have global experience and who master the latest technology.', style: {'font-family': 'avenirNextBold'}
    },
      {
        text: 'Our company is based on making and renovating in the field IOT. We has solved the problem of traingulation by\n' +
        'Bleutooth of a smartphone inside a car that rolls at 30 km/h for a toll company .'
      },
      {
        text: 'We are successfully to solve a problem never solved in the world after a year of research and development .\n' +
        'this solution name Paygate solution .'
      },
      {text: 'Planet connectus'},
      {
        text: 'Do you want to be your partner ? All you need to do is to determine whether or not you want to establish\n' +
        'a foundation for mutual revenue growth. If yes, why the wait? Contact us Today.'
      }], style: {'font-size': '18px', 'letter-spacing': '-0.45px', 'font-family': 'avenirNext'}
  };
  configImageIOTConnectAllThings = {
    src: '/assets/img/planet-connect-us/banners/partners.jpg',
    srcset2x: '/assets/img/planet-connect-us/banners/partners@2x.jpg',
    srcset3x: '/assets/img/planet-connect-us/banners/partners@3x.jpg',
    style: { 'margin-top': '125px'}
  };

  created: boolean = false;
  partnership: Partner = new Partner();
  constructor(private partnerService: PartnerService,private titleService: Title) {
  }

  ngOnInit() {
    this.titleService.setTitle('Partners');
  }

  submit() {
    this.partnerService.createPartner(this.partnership).subscribe(res => {
      this.created = true;
    });
  }

}
