export class Partner {


  firstname: string;
  lastname: string;
  jobTitle: string;
  phone: string;
  email: string;
  company: string;
  country: string;
  contactPurpose: string;
  subject: string;
  message: string;
}
