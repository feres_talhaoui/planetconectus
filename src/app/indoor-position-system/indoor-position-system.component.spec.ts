import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndoorPositionSystemComponent } from './indoor-position-system.component';

describe('IndoorPositionSystemComponent', () => {
  let component: IndoorPositionSystemComponent;
  let fixture: ComponentFixture<IndoorPositionSystemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndoorPositionSystemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndoorPositionSystemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
