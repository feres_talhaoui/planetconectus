import {Component, OnInit} from '@angular/core';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'sit-indoor-position-system',
  templateUrl: './indoor-position-system.component.html',
  styleUrls: ['./indoor-position-system.component.css']
})
export class IndoorPositionSystemComponent implements OnInit {
  container = 'container-fluid';
  simpleImageIndoorPositionSystem = {
    src: '/assets/img/planet-connect-us/banners/ips.png',
    srcset2x: '/assets/img/planet-connect-us/banners/ips@2x.png',
    srcset3x: '/assets/img/planet-connect-us/banners/ips@3x.png',
    style: { 'margin-top': '125px'}
  };




  simpleTextTitle = {
    paragraphs: [{
      text: 'INDOOR POSITINING SYSTEM', style: {
        'letter-spacing': '-0.6px',
        'color': '#72327C',
        'text-align': 'center',
        'font-size': '24px',
        'margin-bottom': '7%'
        ,
        'font-family': 'avenirNextDemi',
        'margin-top': '5%'
      }
    }]
  };
  simpleTextDynamicIndoorNavigationTitle = {
    paragraphs: [{
      text: 'What is Indoor Positioning Systems?', style: {
        'letter-spacing': '-0.5px', 'color': '#EC771B', 'font-family': 'avenirNextBold', 'font-size': '20px'
      }
    }], style: {'color': '#4a4a4a', 'font-family': 'avenirNext', 'font-size': '18px', 'letter-spacing': '-0.45px'}
  };
  simpleTextDynamicIndoorNavigation = {
    paragraphs: [
      {
        text: 'The easiest way to describe Indoor Positioning Systems (IPS) is that it’s like a GPS for indoor environments.'
      },
      {
        text: 'IPS can be used to locate people or objects inside buildings, typically via a mobile device such as a' +
        ' smart phone or tablet. Although the technology is newer than GPS, services that leverage IPS are quickly gaining' +
        ' traction in places like shopping malls, hospitals, airports and other indoor venues where navigation and other' +
        ' location-based services (LBS) can prove to be indispensable.'
      }], style: {'color': '#4a4a4a', 'font-family': 'avenirNext', 'font-size': '18px', 'letter-spacing': '-0.45px'}
  };
  simpleTextLocationBasedNotificationsTitle = {
    paragraphs: [{
      text: 'How does it work?', style: {
        'letter-spacing': '-0.5px', 'color': '#EC771B', 'font-family': 'avenirNextBold', 'font-size': '20px'
      }
    },
      {
        text: 'IPS technology, like StepInside®, leverage the internal sensors in smartphones to calculate the device’s indoor position' +
        ' using complex mathematical algorithms. By combining the incoming data from these sensors in a clever way, a very accurate' +
        ' position can be calculated, with little to no latency, resulting in a smooth user experience.',
      },
      {
        text: 'For the best user experience – one that is both accurate and fast, ' +
        'IPS typically relies on three distinct elements: the underlying dynamic positioning ' +
        'system platform, the beacons that broadcast signals that are picked up by the smartphone ' +
        'and then fed to the positioning system, and the apps built on top of the positioning system' +
        ' that add value and make the systems indispensable to users.'
      }], style: {'color': '#4a4a4a', 'font-family': 'avenirNext', 'font-size': '18px', 'letter-spacing': '-0.45px'}
  };


  simpleTextUseCasesTitle = {
    paragraphs: [{
      text: 'Use Cases\n', style: {
        'letter-spacing': '-0.5px', 'color': '#EC771B', 'font-family': 'avenirNextBold', 'font-size': '20px'
      }
    },
      {
        text: 'Indoor Navigation Apps', style: {
          'letter-spacing': '-0.5px', 'color': '#EC771B', 'font-family': 'avenirNextBold', 'font-size': '20px',

          'margin-top': '-2%'
        }
      }]
  };
  simpleTextUseCases = {
    paragraphs: [
      {
        text: 'Finding your way inside large building complexes such as shopping\n' +
        'malls, airports, rail-stations, exhibition centres, universities, hospitals\n' +
        'and office buildings can be a daunting challenge. With NaviBees\n' +
        'advanced beacon technology and its application program\n' +
        'on your smartphone, it is easy to find your way around.'
      },
      {
        text: 'our application allows facility managers to understand\n' +
        'people’s movements and the ways in which buildings are\n' +
        'being utilized, thereby helping to improve space-usage, perform\n' +
        'location-based targeted marketing campaigns and get unique insights\n' +
        'with advanced business analytics tools.'
      },
    ], style: {'color': '#4a4a4a', 'font-family': 'avenirNext', 'font-size': '18px', 'margin-top': '5%', 'letter-spacing': '-0.45px'}
  };
  simpleImageUseCases = {src: '/assets/img/planet-connect-us/use-case.jpg',
    srcet2x: '/assets/img/planet-connect-us/use-case@2x.jpg',
    srcet3x: '/assets/img/planet-connect-us/use-case@3x.jpg'};


  constructor(private titleService: Title) {
  }

  ngOnInit() {
    this.titleService.setTitle('Solutions - Indoor Position System');
  }

}
