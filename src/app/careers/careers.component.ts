import {Component, OnInit} from '@angular/core';
import {Career} from './Career';
import {CareerService} from './career.service';
import {FileUploader} from 'ng2-file-upload';
import {environment} from '../../environments/environment';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'sit-careers',
  templateUrl: './careers.component.html',
  styleUrls: ['./careers.component.css'],
  providers: [CareerService]
})
export class CareersComponent implements OnInit {
  public loading = false;

  carrersImage = {
    src: '/assets/img/planet-connect-us/banners/careers.jpg',
    srcset2x: '/assets/img/planet-connect-us/banners/careers@2x.jpg',
    srcset3x: '/assets/img/planet-connect-us/banners/careers@3x.jpg',
    style: { 'margin-top': '125px'}
  };
  simpleTextTitle = {
    paragraphs: [ {
      text: 'Join our Team', style: {
        'font-size': '36px', 'font-family': 'avenirNextMedium', 'color': '#72327C', 'letter-spacing': '-0.67px',
        'margin-bottom': '120px'
      }
    }], style: {
      'color': '#814A89', 'text-align': 'center', 'margin-top': '5%',
      'font-size': '36px'
    }
  };
  apiUrl = environment.apiUrl + '/Resumes/resumes/upload';
  public uploader: FileUploader = new FileUploader({
    url: this.apiUrl, allowedMimeType: ['application/pdf', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
      'application/msword']
  });
  errorMessage: string;
  created = false;
  career: Career = new Career();

  constructor(private careerService: CareerService, private titleService: Title) {
  }

  ngOnInit() {
    this.titleService.setTitle('Careers');
  }

  submit() {
    this.loading = true;
    if (this.uploader.queue.length > 0) {
      const lastitem = this.uploader.queue[this.uploader.queue.length - 1];
      lastitem.upload();
      lastitem.onComplete = (response: string, status: number, headers: any) => {
        this.errorMessage = null;
        if (status === 200) {
          const jsonUploadResponse = JSON.parse(response);
          const filename = jsonUploadResponse.result.files.file[0].name;
          this.career.resume = filename;
          this.careerService.createCareer(this.career).subscribe(res => {
            this.loading = false;
            this.created = true;
          });
        }

      };
    } else {
      this.loading = false;
      this.errorMessage = 'The resume must have the format required';

    }
  }

}
