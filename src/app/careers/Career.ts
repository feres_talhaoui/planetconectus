export class Career {
  name: string;
  email: string;
  jobTitle: string;
  resume: string;
}
