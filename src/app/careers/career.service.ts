import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable()
export class CareerService {
  apiUrl = environment.apiUrl;
  constructor(private http: HttpClient) { }

  createCareer(career: any): Observable<any> {
    return this.http.post(this.apiUrl + '/Careers', career);
  }
}
