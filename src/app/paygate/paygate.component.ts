import {Component, OnInit} from '@angular/core';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'sit-paygate',
  templateUrl: './paygate.component.html',
  styleUrls: ['./paygate.component.css']
})
export class PaygateComponent implements OnInit {
  simpleImagePaygate = {
    src: '/assets/img/planet-connect-us/banners/paygate.jpg',
    srcset2x: '/assets/img/planet-connect-us/banners/paygate@2x.jpg',
    srcset3x: '/assets/img/planet-connect-us/banners/paygate@3x.jpg',
    style: { 'margin-top': '125px'}
  };
  simpleTextTitle = {
    paragraphs: [{
      text: 'PAYGATE SOLUTION', style: {
        'letter-spacing': '-0.6px',
        'color': '#72327C',
        'text-align': 'center',
        'font-size': '24px',
        'margin-bottom': '7%'
        ,
        'font-family': 'avenirNextDemi',
        'margin-top': '5%'
      }
    }]
  };
  simpleTextPaygateWorksAsFollow = {
    paragraphs: [{
      text: 'PayGate works as follows.', style: {
        'color': '#EC771B', 'font-family': 'avenirNextBold', 'font-size': '22px'
      }
    },
      {
        text: '1- Download the mobile application (iOS/Android) , through the registration procedures the person will have his own\n' +
        'number under high protection.', style: {'color': '#4a4a4a', 'font-family': 'avenirNext', 'font-size': '18px'}
      },
      {text: '2-Turns on the Bluetooth.', style: {'color': '#4a4a4a', 'font-family': 'avenirNext', 'font-size': '18px'}},
      {
        text: 'Your smartphone has become your key to open all gates compatible with our system.', style: {
          'color': '#4A4A4A', 'font-family': 'avenirNextDemi', 'font-size': '18px',
          'margin-bottom': '3%', 'letter-spacing': '-0.45'
        }
      }]
  };

  simpleTextParagrapNextToImage = {
    paragraphs: [
      {
        text: 'Then when the client passes at a speed of up to 30 km\n' +
        'per hour, our system detect automatically the mobile\n' +
        'inside the vehicle and then open the gate directly in less\n' +
        'than a second and the money is deducted from the\n' +
        'account of the person.'
      },
      {
        text: 'The advantage is the ease of use and put the phone\n' +
        'anywhere in the vehicle even if in the pocket ..\n' +
        'The idea is a patent using Bluetooth.'
      }
    ], style: {'margin-top': '5%', 'color': '#4a4a4a', 'font-family': 'avenirNext', 'font-size': '18px', 'letter-spacing': '-0.45'}
  };
  simpleTextParagrapAfterImage = {
    paragraphs: [
      {
        text: 'You do not need to go to issue an electronic chip or otherwise\n' +
        'just use the mobile with very high protection.'
      },
    ], style: {'color': '#4a4a4a', 'font-family': 'avenirNextDemi', 'font-size': '18px'}
  };
  simpleImage = {
    src: '/assets/img/planet-connect-us/paygate-car.jpg',
    srcet2x: '/assets/img/planet-connect-us/paygate-car@2x.jpg',
    srcet3x: '/assets/img/planet-connect-us/paygate-car@3x.jpg',
    style: {'width': '400px'}
  };

  constructor(private titleService: Title) {
  }

  ngOnInit() {
    this.titleService.setTitle('Solutions - Paygate');
  }

}
