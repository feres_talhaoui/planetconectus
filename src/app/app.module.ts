import {BrowserModule, Title} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import{ReactiveFormsModule} from'@angular/forms'

import {BsDropdownModule} from 'ngx-bootstrap';
import {AppComponent} from './app.component';
import {HomePageComponent} from './home-page/home-page.component';
import {AppRoutingModule} from './app-routing.module';
import {CoreModule} from './sit-cms/core/core.module';
import {ProductModule} from './sit-cms/product/product.module';
import {PaygateComponent} from './paygate/paygate.component';
import {PlanetConnectUsHomePageComponent} from './planet-connect-us-home-page/planet-connect-us-home-page.component';
import {PlanetConnectUsAboutUsComponent} from './planet-connect-us-about-us/planet-connect-us-about-us.component';
import {PlanetConnectUsPlatformComponent} from './planet-connect-us-platform/planet-connect-us-platform.component';
import {PartnersComponent} from './partners/partners.component';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {ContactComponent} from './contact/contact.component';
import {CareersComponent} from './careers/careers.component';
import {FileUploadModule} from 'ng2-file-upload';
import {IndoorPositionSystemComponent} from './indoor-position-system/indoor-position-system.component';
import {LoadingModule} from 'ngx-loading';

import { Angulartics2Module } from 'angulartics2';
import { Angulartics2GoogleAnalytics } from 'angulartics2/ga';
import {BlogComponent}from './blog/blog.component'
import {ArticleService}from '../app/article.service';
import {ArticleComponent} from './article/article.component'
import { Article } from './article';
import {ShowArticleComponent} from './show-article/show-article.component'
/*const staticConfig = {
  header: {
    containerFluidity: true,
    template: 'bootstrap',
    menu: {
      content: [{
        name: 'solutions',
        label: 'Solutions',
        routerLink: '/solutions',
        submenu: [{
          name: 'paygate',
          label: 'Paygate',
          routerLink: '/solutions/paygate',
          style: {'font-family': 'avenirNext', 'font-size': '18px', 'letter-spacing': '-0.45px'}
        },
          {
            name: 'indoor-position-system',
            label: 'Indoor Position System',
            routerLink: '/solutions/indoor-position-system',
            style: { 'font-family': 'avenirNext', 'font-size': '18px', 'letter-spacing': '-0.45px'}
          }]
      },
        {
          name: 'about-us', label: 'About Us', routerLink: '/about', slideImage: '/assets/img/planet-connect-us/aboutus.jpg'
        },
        {
          name: 'partners', label: 'Partners', routerLink: '/partners'
        },
        {
          name: 'platform', label: 'Platform', routerLink: '/platform'
        },
        {
          name: 'contact', label: 'Contact', routerLink: '/contact'
        },
        {
          name: 'careers', label: 'Careers', routerLink: '/careers'
        },
        {name:'blog',label:'Blog',routerLink:'/blog'}
      ],
      style: {'color': '#4a4a4a', 'font-family': 'avenirNext', 'font-size': '18px', 'letter-spacing': '-0.45px'},
      withSlider: true
    },
    logo: {
      src: '/assets/img/planet-connect-us/logo-header.png',
      srcet2x: '/assets/img/planet-connect-us/logo-header-2.png',
      srcet3x: '/assets/img/planet-connect-us/logo-header-3.png',
      routerLink: ''
    }
  },
  footer: {
    template: 'planet-connect-us'
  }
};*/
const staticConfig ={
  header: {
    containerFluidity: true,
    template: 'bootstrap',
    menu: {
      content: [{
        name: 'solutions',
        label: 'Solutions',
        routerLink: '/solutions',
        submenu: [{
          name: 'paygate',
          label: 'Paygate',
          routerLink: '/solutions/paygate',
          style: {'font-family': 'avenirNext', 'font-size': '18px', 'letter-spacing': '-0.45px'}
        },
          {
            name: 'indoor-position-system',
            label: 'Indoor Position System',
            routerLink: '/solutions/indoor-position-system',
            style: { 'font-family': 'avenirNext', 'font-size': '18px', 'letter-spacing': '-0.45px'}
          }]
      },
        /*{
          name: 'about-us', label: 'About Us', routerLink: '/about', slideImage: '/assets/img/planet-connect-us/aboutus.jpg'
        },
        {
          name: 'partners', label: 'Partners', routerLink: '/partners'
        },
        {
          name: 'platform', label: 'Platform', routerLink: '/platform'
        },
        {
          name: 'contact', label: 'Contact', routerLink: '/contact'
        },
        {
          name: 'careers', label: 'Careers', routerLink: '/careers'
        },
        {name:'blog',label:'Blog',routerLink:'/blog'}*/
      ],
      style: {'color': '#4a4a4a', 'font-family': 'avenirNext', 'font-size': '18px', 'letter-spacing': '-0.45px'},
      withSlider: true
    },
    logo: {
      src: '/assets/img/planet-connect-us/logo-header.png',
      srcet2x: '/assets/img/planet-connect-us/logo-header-2.png',
      srcet3x: '/assets/img/planet-connect-us/logo-header-3.png',
      routerLink: ''
    }
  },
  footer: {
    template: 'planet-connect-us'
  }
}

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    PaygateComponent,
    PlanetConnectUsHomePageComponent,
    PlanetConnectUsAboutUsComponent,
    PlanetConnectUsPlatformComponent,
    PartnersComponent,
    ContactComponent,
    CareersComponent,
    IndoorPositionSystemComponent,
  BlogComponent,
    ArticleComponent,
    ShowArticleComponent
],
  imports: [
    Angulartics2Module.forRoot([Angulartics2GoogleAnalytics]),
    BrowserModule.withServerTransition({ appId: 'planetconectus' }),
    CoreModule.forRoot(staticConfig),
    AppRoutingModule,
    BsDropdownModule.forRoot(),
    ProductModule,
    FormsModule,
    HttpClientModule,
    FileUploadModule,
    ReactiveFormsModule,
    LoadingModule.forRoot({
      fullScreenBackdrop: true
    }),

  ],
  providers: [Title,ArticleService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
