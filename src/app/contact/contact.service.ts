import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {environment} from '../../environments/environment';

@Injectable()
export class ContactService {
  apiUrl = environment.apiUrl;
  constructor(private http: HttpClient) { }

  createContact(contact: any): Observable<any> {
    return this.http.post(this.apiUrl + '/Contacts', contact);
  }
}
