import {Component, OnInit} from '@angular/core';
import {Contact} from './Contact';
import {ContactService} from './contact.service';
import {Partner} from '../partners/partner';
import {PartnerService} from '../partners/partner.service';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'sit-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css'],
  providers: [ContactService]
})
export class ContactComponent implements OnInit {
  configImageContactUs = {
    src: '/assets/img/planet-connect-us/banners/contact-us.jpg',
    srcset2x: '/assets/img/planet-connect-us/banners/contact-us@2x.jpg',
    srcset3x: '/assets/img/planet-connect-us/banners/contact-us@3x.jpg',
    style: { 'margin-top': '125px'}
  };
  simpleTextTitle = {
    paragraphs: [{
      text: 'Contact', style: {
        'letter-spacing': '-0.6px',
        'color': '#72327C',
        'text-align': 'center',
        'font-size': '24px',
        'margin-bottom': '100px'
        ,
        'font-family': 'avenirNextDemi',
        'margin-top': '40px'
      }
    }]
  };
  simpleTextContent = {
    paragraphs: [{
      text: 'GET CONNECTED', style: {
        'letter-spacing': '-0.45px',
        'color': '#72327C',
        'font-size': '18px',
        'font-family': 'avenirNextDemi'
      }
    },
      {
        text: 'We are happy to Answer your Questions!', style: {
          'letter-spacing': '-0.45px',
          'color': '#4A4A4A',
          'font-size': '18px',
          'font-family': 'avenirNext',
        }
      }]
  };
  contact: Contact = new Contact();
  created = false;

  constructor(private contactService: ContactService, private titleService: Title) {
  }

  ngOnInit() {
    this.titleService.setTitle('Contact');
  }

  submit() {
    this.contactService.createContact(this.contact).subscribe(res => {
      this.created = true;
    });
  }

}
