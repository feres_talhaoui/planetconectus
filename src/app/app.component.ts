import { Component } from '@angular/core';
import {Angulartics2GoogleAnalytics} from 'angulartics2/ga';

@Component({
  selector: 'sit-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent {
  title = 'app';
  public optionHeader: any;
  public optionFooter: any;

  constructor(angulartics2GoogleAnalytics: Angulartics2GoogleAnalytics) {
    }
  }
