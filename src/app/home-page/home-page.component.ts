import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'sit-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {
  configCallToAction = {
    title: {
      text: 'WHO WE ARE', style: {'color': '#814A89', 'padding-bottom': '30px', 'padding-top': '25px'}
    },
    content: {
      text: ['Planet Conectus is a company specialized in IOT and mobile.\n' +
      '      We accompany compaines to make thier business by realizing POCs and then indutrialize\n' +
      '      the solution.\n',
        '      Planet Conectus possesses one of the fastest and secure connected objects in the world']
    },
    button: {
      text: 'Learn more', style: {
        'background-color': '#EA771C',
        'float': 'right',
        'color': 'white'
      }
    },
  };
  configSimpleText = {
    paragraphs: ['DSI est le leader sur le marché tunisien de la maintenance conditionnelle et de la surveillance des machines\n' +
    '    tournantes, ses ingénieurs et techniciens assistent quotidiennement les industriels dans la prise de décisions en ce\n' +
    '    qui concerne le diagnostic et la maintenance de leurs outils de production.',
      'Sa mission est d’étudier, fournir et mettre en application les produits de la maintenance prévisionnelle basée sur\n' +
      '    la fiabilité, les services et la formation afin d’améliorer la rentabilité des industries et apporter des solutions\n' +
      '    innovantes.',
      ' DSI propose une large gamme de capteurs et d’instruments de mesure et de surveillance, dans les domaines:\n' +
      '    vibrations, thermographie, bruit, CND, Process, analyse et protection réseaux électriques,…',
      ' Forte de son expérience, DSI est présente dans une variété d’industries comme la production d’énergie, la\n' +
      '    pétrochimie, l’industrie de ciment et de céramique, l’agroalimentaire...',
      ' DSI partage son savoir faire et son expérience à travers l’animation de formations dans tous ses domaines de\n' +
      '    compétence, en intra comme en interentreprises.'], style: {'margin-top': '5%', 'margin-bottom': '10%'}
  };
  configSimpleImage = {
    src: '/assets/img/planet-connect-us/home.png',
    srcset2x: '/assets/img/planet-connect-us/home-2.png',
    srcset3x: '/assets/img/planet-connect-us/home-3.png', style: {'margin-top': '85px'}
  };

  constructor() {
  }

  ngOnInit() {
  }

}
