import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'sit-product-image-widget',
  templateUrl: './product-image-widget.component.html',
  styleUrls: ['./product-image-widget.component.css']
})
export class ProductImageWidgetComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
