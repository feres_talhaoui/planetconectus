import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductImageWidgetComponent } from './product-image-widget.component';

describe('ProductImageWidgetComponent', () => {
  let component: ProductImageWidgetComponent;
  let fixture: ComponentFixture<ProductImageWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductImageWidgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductImageWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
