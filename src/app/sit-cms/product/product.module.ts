import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {WidgetProductComponent} from './widget-product.component';
import {ProductImageWidgetComponent} from './product-image-widget/product-image-widget.component';

@NgModule({
  imports: [ CommonModule ],
  declarations: [ WidgetProductComponent, ProductImageWidgetComponent ],
  exports: [ WidgetProductComponent, ProductImageWidgetComponent ]
})
export class ProductModule {}
