import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterUnifyComponent } from './footer-unify.component';

describe('FooterUnifyComponent', () => {
  let component: FooterUnifyComponent;
  let fixture: ComponentFixture<FooterUnifyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FooterUnifyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterUnifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
