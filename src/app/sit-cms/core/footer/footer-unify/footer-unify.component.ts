import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'sit-footer-unify',
  templateUrl: './footer-unify.component.html',
  styleUrls: ['./footer-unify.component.css']
})
export class FooterUnifyComponent implements OnInit {
  @Input() config: any;
  constructor() { }

  ngOnInit() {
  }

}
