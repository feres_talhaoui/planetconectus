import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'sit-footer-planet-connect-us',
  templateUrl: './footer-planet-connect-us.component.html',
  styleUrls: ['./footer-planet-connect-us.component.css']
})
export class FooterPlanetConnectUsComponent implements OnInit {
  @Input() config: any;
  public today: any = Date.now();

  constructor() { }
  simpleImageLogoFooter = {
    src: 'assets/img/planet-connect-us/logo-footer.jpg',
    srcet2x: 'assets/img/planet-connect-us/logo-footer-2.png',
    srcet3x: 'assets/img/planet-connect-us/logo-footer-3.png'
  };

  ngOnInit() {
  }

}
