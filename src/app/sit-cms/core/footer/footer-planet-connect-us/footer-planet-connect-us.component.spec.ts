import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterPlanetConnectUsComponent } from './footer-planet-connect-us.component';

describe('FooterPlanetConnectUsComponent', () => {
  let component: FooterPlanetConnectUsComponent;
  let fixture: ComponentFixture<FooterPlanetConnectUsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FooterPlanetConnectUsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterPlanetConnectUsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
