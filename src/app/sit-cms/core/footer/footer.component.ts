import {Component, Input, OnInit} from '@angular/core';
import {ConfigService} from '../config/config.service';

@Component({
  selector: 'sit-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  @Input() option: any = {};
  config: any =  {};
  constructor(public configService: ConfigService) { }

  ngOnInit() {
    const menuConfig = this.configService.footer();
    this.config.template = menuConfig.template;
  }

}
