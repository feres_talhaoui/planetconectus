import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'sit-simple-image',
  templateUrl: './simple-image.component.html',
  styleUrls: ['./simple-image.component.css']
})
export class SimpleImageComponent implements OnInit {
  @Input() config: any;
  srcset: string;
  constructor() { }

  ngOnInit() {
    if (this.config.srcset2x && this.config.srcset3x) {
      this.srcset = this.config.srcset2x + ' 2x, ' + this.config.srcset3x + ' 3x';
    }
  }

}
