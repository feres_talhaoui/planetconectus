import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PaygateComponent} from '../../paygate/paygate.component';

const routes: Routes = [
  { path: 'paygate', component: PaygateComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class CoreRoutingModule { }
