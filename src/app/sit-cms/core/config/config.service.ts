import {Injectable, Optional} from '@angular/core';

export class ModuleServiceConfig {
  header:  {
  containerFluidity: boolean;
  template: string;}
}

@Injectable()
export class ConfigService {
  config: any;

  constructor(@Optional() config: ModuleServiceConfig) {
    if (config) {

      this.config = config;
      if (config.header.containerFluidity) {
        this.config.header = Object.assign({containerType: 'container-fluid'}, this.config.header);
      } else {
        this.config.header = Object.assign({containerType: 'container'}, this.config.header);
      }
    }
  }

  header() {
    return this.config.header;
  }
  footer() {
    return this.config.footer;
  }

}
