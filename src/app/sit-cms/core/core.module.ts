import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HeaderComponent} from './header/header.component';
import {FooterComponent} from './footer/footer.component';
import {HeaderUnifyComponent} from './header/header-unify/header-unify.component';
import {HeaderBootstrapComponent} from './header/header-bootstrap/header-bootstrap.component';
import {BsDropdownModule} from 'ngx-bootstrap';
import {ConfigService, ModuleServiceConfig} from './config/config.service';
import {SimpleImageComponent} from './simple-image/simple-image.component';
import {SimpleWidgetComponent} from './simple-widget/simple-widget.component';
import {SimpleSlideComponent} from './simple-slide/simple-slide.component';
import {SimpleTextComponent} from './simple-text/simple-text.component';
import {CoreRoutingModule} from './core-routing.module';
import {RouterModule} from '@angular/router';
import {CallToActionComponent} from './call-to-action/call-to-action.component';
import {FooterPlanetConnectUsComponent} from './footer/footer-planet-connect-us/footer-planet-connect-us.component';
import {FooterUnifyComponent} from './footer/footer-unify/footer-unify.component';
import {RichTextComponent} from './rich-text/rich-text.component';
import {SafeHtml} from '../utils/safehtml.pipe';

@NgModule({
  imports: [CommonModule,
    RouterModule,
    BsDropdownModule.forRoot(),
  CoreRoutingModule],
  declarations: [HeaderComponent,
    FooterComponent,
    HeaderUnifyComponent,
    HeaderBootstrapComponent,
    SimpleSlideComponent,
    SimpleTextComponent,
    SimpleImageComponent,
    SimpleWidgetComponent,
    CallToActionComponent,
    FooterPlanetConnectUsComponent,
    FooterUnifyComponent,
    RichTextComponent,
    SafeHtml
  ],
  exports: [HeaderComponent,
    FooterComponent,
    HeaderBootstrapComponent,
    SimpleSlideComponent,
    SimpleTextComponent,
    SimpleImageComponent,
    SimpleWidgetComponent,
    CallToActionComponent,
    RichTextComponent,
    FooterPlanetConnectUsComponent],
  providers: [ConfigService]
})
export class CoreModule {
  static forRoot(config: ModuleServiceConfig): ModuleWithProviders {
    return {
      ngModule: CoreModule,
      providers: [
        {provide: ModuleServiceConfig, useValue: config}
      ]
    };
  }
}
