import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'sit-rich-text',
  templateUrl: './rich-text.component.html',
  styleUrls: ['./rich-text.component.css']
})
export class RichTextComponent implements OnInit {

  @Input() config: any;
  constructor() { }

  ngOnInit() {
  }

}
