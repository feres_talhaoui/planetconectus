import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'sit-simple-text',
  templateUrl: './simple-text.component.html',
  styleUrls: ['./simple-text.component.css']
})
export class SimpleTextComponent implements OnInit {

  @Input() config: any;
  constructor() { }

  ngOnInit() {
  }

}
