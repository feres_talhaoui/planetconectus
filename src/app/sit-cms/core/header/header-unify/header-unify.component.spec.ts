import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderUnifyComponent } from './header-unify.component';

describe('HeaderUnifyComponent', () => {
  let component: HeaderUnifyComponent;
  let fixture: ComponentFixture<HeaderUnifyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderUnifyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderUnifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
