import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'sit-header-unify',
  templateUrl: './header-unify.component.html',
  styleUrls: ['./header-unify.component.css']
})
export class HeaderUnifyComponent implements OnInit {


  @Input() config: any;

  constructor() {
  }

  collapsed = true;

  toggleCollapsed(): void {
    this.collapsed = !this.collapsed;
  }


  onHidden(): void {
  }

  onShown(): void {
  }

  isOpenChange(): void {
  }

  ngOnInit() {
    if (this.config.menu.content) {
      for (let i in this.config.menu.content) {
        if (this.config.menu.style && this.config.menu.content[i].style) {
          let menuStyle = Object.assign({}, this.config.menu.style);

          this.config.menu.content[i].style = Object.assign(menuStyle, this.config.menu.content[i].style);
        }
        else if (this.config.menu && !this.config.menu.content[i].style) {
          this.config.menu.content[i].style = this.config.menu.style;
        }
      }
    }
  }
}
