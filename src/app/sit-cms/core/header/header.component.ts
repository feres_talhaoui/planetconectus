import {Component, Input, OnInit} from '@angular/core';
import {ConfigService} from '../config/config.service';

@Component({
  selector: 'sit-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  providers: [ConfigService]
})
export class HeaderComponent implements OnInit {
  @Input() option: any = {};
  config: any =  {};


  constructor(public configService: ConfigService) {
  }

  ngOnInit() {
    const menuConfig = this.configService.header();
    this.config.containerType = menuConfig.containerType;
    this.config.template = menuConfig.template;
    this.config.logo = (menuConfig.logo) ? menuConfig.logo : this.option.logo;
    this.config.menu = (menuConfig.menu) ? menuConfig.menu : this.option.menu;
    

  }

}
