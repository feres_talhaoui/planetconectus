import { Component, ElementRef, HostListener, Inject, Input, OnInit, PLATFORM_ID, Renderer2, ViewChild } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { isPlatformBrowser } from '@angular/common';
import { HttpClient } from '@angular/common/http'
@Component({
  selector: 'sit-header-bootstrap',
  templateUrl: './header-bootstrap.component.html',
  styleUrls: ['./header-bootstrap.component.css']
})
export class HeaderBootstrapComponent implements OnInit {


  srcet: string;
  @Input() config: any;
  @ViewChild('logo') logoElement: ElementRef;
  @ViewChild('menucontainerlist') menuContainerListElement: ElementRef;
  @ViewChild('toggleresponsivemenubutton') toggleResponsiveMenuButtonElement: ElementRef;
  menuActiveLinks: any[] = [];
  collapsed = true;
  public docElem: any;
  public changeHeaderOn = 200;
  public didScroll = false;
  constructor(public router: Router, @Inject(PLATFORM_ID) private platformId: Object, public renderer: Renderer2, public http: HttpClient) {

/*
    if (isPlatformBrowser(this.platformId)) {
      this.docElem = document.documentElement;
    }

    router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        for (let i in this.config.menu.content) {
          if (event.url.indexOf(this.config.menu.content[i].routerLink) !== -1) {
            this.menuActiveLinks[this.config.menu.content[i].name]['active'] = true;
          }
          else {
            this.menuActiveLinks[this.config.menu.content[i].name]['active'] = false;
          }

          if (this.config.menu.content[i].submenu) {
            for (let j in this.config.menu.content[i].submenu) {
              if (event.url.indexOf(this.config.menu.content[i].submenu[j].routerLink) !== -1) {
                this.menuActiveLinks[this.config.menu.content[i].name][this.config.menu.content[i].submenu[j].name] = true;
              }
              else {
                this.menuActiveLinks[this.config.menu.content[i].name][this.config.menu.content[i].submenu[j].name] = false;
              }
            }
          }
        }

        if (isPlatformBrowser(this.platformId)) {
          window.scrollTo(0, 0);
        }

      }

    });

  }
  @HostListener('window:scroll') onWindowScroll() {

    if (!this.didScroll) {
      this.didScroll = true;
      if (isPlatformBrowser(this.platformId)) {
        this.scrollPage();
      }
    }
  }

  public scrollPage(): any {

    const sy = this.scrollY();
    if (sy >= this.changeHeaderOn) {
      this.renderer.addClass(this.logoElement.nativeElement, 'logo-scroll');
      this.renderer.addClass(this.menuContainerListElement.nativeElement, 'menu-container-scroll');
      this.renderer.addClass(this.toggleResponsiveMenuButtonElement.nativeElement, 'toggle-responsive-scroll');
    } else {
      if (this.logoElement !== undefined) {
        this.renderer.removeClass(this.logoElement.nativeElement, 'logo-scroll');
        this.renderer.removeClass(this.menuContainerListElement.nativeElement, 'menu-container-scroll');
        this.renderer.removeClass(this.toggleResponsiveMenuButtonElement.nativeElement, 'toggle-responsive-scroll');
      }
    }
    this.didScroll = false;
  }

  public scrollY() {
    if (isPlatformBrowser(this.platformId)) {
      return window.pageYOffset || this.docElem.scrollTop;
    }
  }


  toggleCollapsed(): void {
    this.collapsed = !this.collapsed;
  }

  onMenuItemClicked(): void {

    this.toggleCollapsed();
*/
  }
 
  ngOnInit() {
    this.http.get("http://localhost:3001/menus").subscribe((res: Array<any>) => {
      //console.log("res", res)
      res.sort((a: any, b: any) => {
        return a.index - b.index

      })
      console.log("resor", res)
      for (let item of res) {
        let { name, label, routerLink, index } = item;
        //console.log(name, " ", label, " ", routerLink)
        //console.log(this.config.menu.content)
        this.config.menu.content.push({ name, label, routerLink, index })
        //Object.assign(this.config.menu,{name,label,routerLink})
      }
      if (this.config.menu.content) {
        for (let i in this.config.menu.content) {
          if (this.config.menu.style && this.config.menu.content[i].style) {
            const menuStyle = Object.assign({}, this.config.menu.style);
            this.config.menu.content[i].style = Object.assign(menuStyle, this.config.menu.content[i].style);
          }
          else if (this.config.menu.style && !this.config.menu.content[i].style) {
            this.config.menu.content[i].style = this.config.menu.style;
          }
          this.menuActiveLinks[this.config.menu.content[i].name] = [];
          // this.menuActiveLinks[this.config.menu.content[i].name]['active'] = false;
          /*if (this.config.menu.content[i].submenu) {
            for (let j in this.config.menu.content[i].submenu) {
              this.menuActiveLinks[this.config.menu.content[i].name][this.config.menu.content[i].submenu[j].name] = false;
            }
          }*/
        }
      }


      if (this.config.logo.srcet2x && this.config.logo.srcet3x) {
        this.srcet = this.config.logo.srcet2x + ' 2x, ' + this.config.logo.srcet3x + ' 3x';
      }
    })

    /*  if (this.config.menu.content) {
        for (let i in this.config.menu.content) {
          if (this.config.menu.style && this.config.menu.content[i].style) {
            const menuStyle = Object.assign({}, this.config.menu.style);
            this.config.menu.content[i].style = Object.assign(menuStyle, this.config.menu.content[i].style);
          }
          else if (this.config.menu.style && !this.config.menu.content[i].style) {
            this.config.menu.content[i].style = this.config.menu.style;
          }
          this.menuActiveLinks[this.config.menu.content[i].name] = [];
          this.menuActiveLinks[this.config.menu.content[i].name]['active'] = false;
          if (this.config.menu.content[i].submenu) {
            for (let j in this.config.menu.content[i].submenu) {
              this.menuActiveLinks[this.config.menu.content[i].name][this.config.menu.content[i].submenu[j].name] = false;
            }
          }
        }
      }
  
  
      if (this.config.logo.srcet2x && this.config.logo.srcet3x) {
        this.srcet = this.config.logo.srcet2x + ' 2x, ' + this.config.logo.srcet3x + ' 3x';
      }*/

  }
}
