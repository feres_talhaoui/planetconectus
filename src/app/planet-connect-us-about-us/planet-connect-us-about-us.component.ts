import {Component, OnInit} from '@angular/core';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'sit-planet-connect-us-about-us',
  templateUrl: './planet-connect-us-about-us.component.html',
  styleUrls: ['./planet-connect-us-about-us.component.css']
})
export class PlanetConnectUsAboutUsComponent implements OnInit {

  simpleImageAboutUs = {
    src: '/assets/img/planet-connect-us/banners/about-us.jpg',
    srcset2x: '/assets/img/planet-connect-us/banners/about-us@2x.jpg',
    srcset3x: '/assets/img/planet-connect-us/banners/about-us@3x.jpg',
    style: { 'margin-top': '125px'}
  };

  constructor(private titleService: Title) {

  }

  ngOnInit() {
    this.titleService.setTitle('About us');
  }

}
